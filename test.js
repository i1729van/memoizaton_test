const memoization = require("./memoizaton");
const expect = require("chai").expect;
const sinon = require("sinon");
var clock = sinon.useFakeTimers();
// hint: use https://sinonjs.org/releases/v6.1.5/fake-timers/ for faking timeouts
after(function() {
  clock.restore();
});
describe("memoization", function() {
  it("should memoize function result", () => {
    let returnValue = 5;
    const testFunction = key => returnValue;

    const memoized = memoization.memoize(testFunction, key => key, 1000);
    expect(memoized("c544d3ae-a72d-4755-8ce5-d25db415b776")).to.equal(5);

    returnValue = 10;
    clock.tick(999);
    // TODO currently fails, should work after implementing the memoize function, it should also work with other
    // types then strings, if there are limitations to which types are possible please state them
    expect(memoized("c544d3ae-a72d-4755-8ce5-d25db415b776")).to.equal(5);
    clock.tick(2);
    expect(memoized("c544d3ae-a72d-4755-8ce5-d25db415b776")).to.equal(10);
    returnValue = 11;
    clock.tick(1001);
    expect(memoized("c544d3ae-a72d-4755-8ce5-d25db415b776")).to.equal(11);
  });

  it("should memoize function result by Number as key", () => {
    let returnValue = 5;
    const testFunction = key => returnValue;

    const memoized = memoization.memoize(testFunction, key => key, 1000);
    expect(memoized(123)).to.equal(5);
    returnValue = 6;
    clock.tick(999);
    expect(memoized(123)).to.equal(5); // should be 5 cause timeout not ended yet
    expect(memoized(123)).not.equal(6); // same
    expect(memoized(777)).to.equal(6); // should be processed with other key (777) and new returnValue(6)
    clock.tick(2); // tick another 2 ms --> should reach timeout end
    expect(memoized(123)).not.equal(5); // shoud be processed with new returnValue (6)
    expect(memoized(123)).to.equal(6); // shoud be processed with new returnValue (6)
  });

  it("memo with null-key", () => {
    let returnValue = 5; //
    const testFunction = key => returnValue;

    const memoized = memoization.memoize(testFunction, key => key, 1000);
    expect(memoized(null)).to.equal(5); // should be work too. in JS null is object too :)
    returnValue = "ads";
    expect(memoized("new_key")).to.equal("ads"); // new_key key with new value "ads"
    expect(memoized(null)).to.equal(5); // but null-key with old value "5"
    clock.tick(1000 + 1); // reach timeout
    expect(memoized(null)).to.equal("ads"); // null-key value whould be equal with new string
  });

  it("test with function from comment-section :)", () => {
    let addToTime = (year, month, day) => {
      return Date.now() + Date(year, month, day);
    };

    const memoized = memoization.memoize(
      addToTime,
      (year, month, day) => year + month + day,
      5000
    );

    // call the provided function cache the result and return the value
    const result = memoized(1, 11, 26); // result = 1534252012350
    // because there was no timeout this call should return the memorized value from the first call
    const secondResult = memoized(1, 11, 26); // secondResult = 1534252012350
    clock.tick(5000); // reach timeout with fakeTimeouts
    // after 5000 ms the value is not valid anymore and the original function should be called again
    const thirdResult = memoized(1, 11, 26); // thirdResult = 1534252159271
  });
});
