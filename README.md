
[![pipeline status](https://gitlab.com/i1729van/memoizaton_test/badges/master/pipeline.svg)](https://gitlab.com/i1729van/memoizaton_test/commits/master)
# Memoization

A general introduction into memoization: https://en.wikipedia.org/wiki/Memoization

See memoization.js and test.js for the required functionality. Please provide a
design rationale documenting your decisions (in code).